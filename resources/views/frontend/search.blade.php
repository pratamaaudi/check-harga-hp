<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Bolt - Coming Soon Template</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}" > 
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="{{asset('fonts/line-icons.css')}}">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/slicknav.css')}}">
    <!-- Off Canvas Menu -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/menu_sideslide.css')}}">
    <!-- Color Switcher -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/vegas.min.css')}}">
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/animate.css')}}">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/responsive.css')}}">

  </head>
  <body>

    <div class="bg-wraper overlay has-vignette">
      <div id="example"  class="slider opacity-50 vegas-container" style="height: 983px; filter: blur(8px);"></div>
    </div>

    <!-- Coundown Section Start -->
    <section class="countdown-timer">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8 col-md-12 col-xs-12 text-center">
            <div class="heading-count">
              <h2>Search Here</h2>
              <p class="text-light">Search For Better Phone at the Right Price</p>
            </div>
          </div>
          <div class="col-lg-8 col-md-12 col-xs-12">
              <div class="subscribe-form text-center">
                <form action="{{url('/samsung-galaxy-s7')}}">
                  <input class="form-control btn-rounded text-dark" placeholder="Ex : Samsung Galaxy S7" type="text">
                </form>
              </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Coundown Section End -->

    <!-- Preloader -->
    <div id="preloader">
      <div class="loader" id="loader-1"></div>
    </div>
    <!-- End Preloader -->

   <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('js/jquery-min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/vegas.min.js')}}"></script>
    <script src="{{asset('js/jquery.countdown.min.js')}}"></script>
    <script src="{{asset('js/classie.js')}}"></script>
    <script src="{{asset('js/jquery.nav.js')}}"></script>
    <script src="{{asset('js/jquery.easing.min.js')}}"></script>
    <script src="{{asset('js/wow.js')}}"></script>
    <script src="{{asset('js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="{{asset('js/form-validator.min.js')}}"></script>
    <script src="{{asset('js/contact-form-script.min.js')}}"></script>

    <script type="text/javascript">
      $("#example").vegas({
          timer: false,
          delay: 6000,
          transitionDuration: 2000,
          transition: "blur",
          slides: [
              { src: "{{asset('img/asset/bg01.jpg')}}" }
          ]
      });
    </script>
      
  </body>
</html>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">

  {{-- select 2 style --}}
  <link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/select2-bootstrap.min.css')}}" rel="stylesheet">

  {{-- daterangepicker --}}
  <link href="{{asset('css/daterangepicker.css')}}" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.html">Start Bootstrap</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bell fa-fw"></i>
          <span class="badge badge-danger">9+</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-envelope fa-fw"></i>
          <span class="badge badge-danger">7</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        {{-- <a class="nav-link" href="{{ url('/') }}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>HOME PAGE</span>
        </a> --}}
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">ADMIN MENU:</h6>
          <a class="dropdown-item" href="{{ url('/admin/add-product') }}">ADD PHONE</a>
          <a class="dropdown-item" href="{{ url('/admin/add-data') }}">SHOW DATA</a>
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Other Pages:</h6>
          <a class="dropdown-item" href="404.html">404 Page</a>
          <a class="dropdown-item" href="blank.html">Blank Page</a>
        </div>
      </li>
    </ul>

    <div id="content-wrapper">
        <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Admin</a>
            </li>
            <li class="breadcrumb-item active">Data</li>
        </ol>

        <div class="row mb-3">
            <div class="col-12">
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-filter"></i> Filter By <button class="btn btn-primary float-right" type="button"><i class="fas fa-cog"></i></button>
                    </div>

                    <div class="card-body">
                      <div class="row">

                        <div class="col-lg-3 col-md-5 col-12 mb-2 mb-lg-0 mb-md-0">
                            <select id="phoneManufacture" class="form-control select2">
                                <option>Select Manufacture</option>
                                <option>Samsung</option>
                                <option>Xiaomi</option>
                                <option>OPPO</option>
                            </select> 
                        </div>

                        {{-- phone name --}}
                        <div class="col-lg-3 col-md-5 col-12 mb-2 mb-lg-0 mb-md-0">
                            <select id="phoneName" class="form-control select2">
                                <option>Select Phone</option>
                                <option>Samsung Galaxy S7</option>
                                <option>Samsung Galaxy S8</option>
                                <option>Samsung Galaxy S9</option>
                            </select> 
                        </div>

                        <div class="col-lg-2 col-md-5 col-12">
                            <select id="phoneVariant" class="form-control select2">
                                <option>Select Variant</option>
                                <option>Edge</option>
                                <option>Limited Edition</option>
                            </select> 
                        </div>

                        <div class="col-lg-3 col-md-5 col-12">
                            <input type="text" id="createdAt" class="form-control">
                        </div>

                        <div class="col-lg-1">
                          <button class="btn btn-success btn-block" type="button"><i class="fas fa-search"></i></button>
                        </div>

                      </div>
                    </div>
                </div>

                {{-- Variant Detail --}}
                <div class="card mb-3">
                    <div class="card-header">
                        <i class="fas fa-table"></i> Data 
                        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#modalAddData">
                          <i class="fas fa-plus"></i>
                        </button>
                        <label class="btn btn-success float-right mr-2">
                            <i class="fas fa-file-excel"></i> <input type="file" hidden>
                        </label>
                    </div>
                    <div class="card-body">
                        <table class="table">
                          <thead>
                            <tr>
                              <th>Manufacture</th>
                              <th>Phone Name</th>
                              <th>Release Date</th>
                              <th>Data Count</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>Samsung</td>
                              <td>Galaxy s7 (Edge)</td>
                              <td>2015</td>
                              <td><a href="#" data-toggle="modal" data-target="#modelListPhone">15</a></td>
                            </tr>
                            <tr>
                              <td>Samsung</td>
                              <td>Galaxy s7</td>
                              <td>2015</td>
                              <td><a href="#" data-toggle="modal" data-target="#modelListPhone">20</a></td>
                            </tr>
                            <tr>
                              <td>Samsung</td>
                              <td>Galaxy s7 (Limited Edition)</td>
                              <td>2015</td>
                              <td><a href="#" data-toggle="modal" data-target="#modelListPhone">23</a></td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->

        <!-- Sticky Footer -->
        <footer class="sticky-footer">
          <div class="container my-auto">
            <div class="copyright text-center my-auto">
              <span>Copyright © Your Website 2019</span>
            </div>
          </div>
        </footer>

      </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" id="modelListPhone" tabindex="-1">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Phone List</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <table class="table table-hover">
                <tbody>
                  <tr class="phoneList" data-url="{{ url('/samsung-galaxy-s7/123') }}">
                    <td>21 Apr 2019</td>
                    <td>5.000.000</td>
                  </tr>
                  <tr class="phoneList" data-url="{{ url('/samsung-galaxy-s7/123') }}">
                    <td>21 Apr 2019</td>
                    <td>5.000.000</td>
                  </tr>
                  <tr class="phoneList" data-url="{{ url('/samsung-galaxy-s7/123') }}">
                    <td>21 Apr 2019</td>
                    <td>5.000.000</td>
                  </tr>
                  <tr class="phoneList" data-url="{{ url('/samsung-galaxy-s7/123') }}">
                    <td>21 Apr 2019</td>
                    <td>5.000.000</td>
                  </tr>
                  <tr class="phoneList" data-url="{{ url('/samsung-galaxy-s7/123') }}">
                    <td>21 Apr 2019</td>
                    <td>5.000.000</td>
                  </tr>
                  <tr class="phoneList" data-url="{{ url('/samsung-galaxy-s7/123') }}">
                    <td>21 Apr 2019</td>
                    <td>5.000.000</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

    <div class="modal fade" id="modalAddData" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalAddData">Add Data</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">

              <div class="row">
                <div class="col-3">
                  <img class="img-fluid h-100 w-2" src="{{asset('storage/asset/phone_placeholder.jpg')}}">
                </div>
                <div class="col-9">
                    <div class="mb-2">
                        <select id="phoneManufactureAddData" class="form-control select2 mb-3">
                            <option>Select Manufacture</option>
                            <option>Samsung</option>
                            <option>Xiaomi</option>
                            <option>OPPO</option>
                        </select>
                    </div>
      
                    <div class="mb-2">
                        <select id="phoneNameAddData" class="form-control select2">
                            <option>Select Phone</option>
                            <option>Samsung Galaxy S7</option>
                            <option>Samsung Galaxy S8</option>
                            <option>Samsung Galaxy S9</option>
                        </select> 
                    </div>
      
                    <div class="mb-2">
                        <select id="phoneVariantAddData" class="form-control select2">
                            <option>Select Variant</option>
                            <option>Edge</option>
                            <option>Limited Edition</option>
                        </select> 
                    </div>
      
                    <div class="mb-2">
                        <select id="kondisi" class="form-control select2">
                            <option>Sekond</option>
                            <option>New</option>
                        </select> 
                    </div>
      
                    <div class="mb-2">
                        <input type="url" placeholder="Link" class="form-control">
                    </div>
      
                    <div class="mb-2">
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Rp.</span>
                          </div>
                          <input type="text" class="form-control" placeholder="Price">
                        </div>
                    </div>
                </div>
              </div>

              <div id="deviceCondition">
                <hr>
                <h2>Device Condition</h2>
                <hr>

                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Part : </span>
                    </div>
                    <select id="displayCondition" class="form-control">
                        <option selected>Display</option>
                        <option>Speaker</option>
                    </select> 
                    <div class="input-group-prepend">
                      <span class="input-group-text">Condition : </span>
                    </div>
                    <select id="displayCondition" class="form-control">
                        <option>Retak</option>
                        <option>Shadow</option>
                    </select> 
                    <div class="input-group-append">
                      <button class="btn btn-outline-info" type="button">F</button>
                    </div>
                    <div class="input-group-append">
                      <button class="btn btn-outline-danger" type="button">X</button>
                    </div>
                </div>

                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <span class="input-group-text">Part : </span>
                    </div>
                    <select id="displayCondition" class="form-control">
                        <option>Display</option>
                        <option selected>Speaker</option>
                    </select> 
                    <div class="input-group-prepend">
                      <span class="input-group-text">Condition : </span>
                    </div>
                    <select id="displayCondition" class="form-control">
                        <option>Rusak</option>
                        <option>Kecil</option>
                    </select> 
                    <div class="input-group-append">
                      <button class="btn btn-outline-info" type="button">F</button>
                    </div>
                    <div class="input-group-append">
                      <button class="btn btn-outline-danger" type="button">X</button>
                    </div>
                </div>
              </div>

              <button id="btnAddCondition" class="btn btn-success float-right" onclick="addDeviceCondition()">+</button>
            </div>  
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Add</button>
            </div>
          </div>
        </div>
      </div>

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ url('/samsung-galaxy-s7') }}">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Page level plugin JavaScript-->
  <script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{asset('js/sb-admin.min.js')}}"></script>

  <!-- Demo scripts for this page-->
  <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
  <script src="{{asset('js/demo/chart-area-demo.js')}}"></script>

  {{-- select 2 js --}}
  <script src="{{asset('js/select2.min.js')}}"></script>

  {{-- daterangepicker --}}
  <script src="{{asset('js/moment.min.js')}}"></script>
  <script src="{{asset('js/daterangepicker.js')}}"></script>

  <script>
    $(document).ready(function() {
        $('.select2').select2({
            theme: "bootstrap",
            width: '100%'
        })

        $('#createdAt').daterangepicker()
    });

    $('#kondisi').on('change', function(){
      toggleDeviceCondition($('#kondisi option:selected').text())
    })

    $('.phoneList').click(function(){
      let phoneUrl = $(this).data("url")
      window.location = phoneUrl;
    })

    function toggleDeviceCondition(kondisi) {
      if(kondisi == 'New'){
        $('#deviceCondition').hide();
        $('#btnAddCondition').hide();
      }else{
        $('#deviceCondition').show();
        $('#btnAddCondition').show();
      }
    }

    function addDeviceCondition(){
      $('#deviceCondition').append('<div class="input-group mb-2"><div class="input-group-prepend"><span class="input-group-text">Part : </span></div><select id="displayCondition" class="form-control"><option>Display</option><option selected>Speaker</option></select><div class="input-group-prepend"><span class="input-group-text">Condition : </span></div><select id="displayCondition" class="form-control"><option>Rusak</option><option>Kecil</option></select><div class="input-group-append"><button class="btn btn-outline-info" type="button">F</button></div><div class="input-group-append"><button class="btn btn-outline-danger" type="button">X</button></div></div>')
    }
  </script>

</body>

</html>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin - Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="{{asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{asset('css/sb-admin.css')}}" rel="stylesheet">

  {{-- select 2 style --}}
  <link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/select2-bootstrap.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.html">Start Bootstrap</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bell fa-fw"></i>
          <span class="badge badge-danger">9+</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-envelope fa-fw"></i>
          <span class="badge badge-danger">7</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        {{-- <a class="nav-link" href="{{ url('/') }}">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>HOME PAGE</span>
        </a> --}}
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-fw fa-folder"></i>
          <span>Pages</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">
            ADMIN MENU:
          </h6>
          <a class="dropdown-item" href="{{ url('/admin/add-product') }}">ADD PHONE</a>
          <a class="dropdown-item" href="{{ url('/admin/add-data') }}">SHOW DATA</a>
          <div class="dropdown-divider"></div>
          <h6 class="dropdown-header">Other Pages:</h6>
          <a class="dropdown-item" href="404.html">404 Page</a>
          <a class="dropdown-item" href="blank.html">Blank Page</a>
        </div>
      </li>
    </ul>

    <div id="content-wrapper">
        <form>
            <div class="container-fluid">

            <!-- Breadcrumbs-->
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Admin</a>
                </li>
                <li class="breadcrumb-item active">Product Detail</li>
            </ol>

            <div class="row mb-3">
                {{-- phone upload menu --}}
                <div class="col-12 col-lg-3 mb-3">
                    <div class="card">
                        <div class="card-header">
                            <i class="fa fa-mobile"></i> Phone Model
                        </div>
                        <div class="card-body text-center">
                            <img class="img-fluid h-100 w-2" src="{{asset('storage/asset/phone_placeholder.jpg')}}">
                        </div>
                        @if ($mode != 'show')
                          <div class="card-footer small text-muted">
                            <label class="btn btn-primary btn-block">
                                <i class="fas fa-upload"></i> Upload <input type="file" hidden>
                            </label>
                          </div>
                        @endif
                    </div>
                </div>

                {{-- phone detail --}}
                <div class="col-12 col-lg-9">
                    <div class="card mb-3">
                        <div class="card-header">
                            <i class="fas fa-info-circle"></i> Phone General Information
                        </div>

                        <div class="card-body">
                            <div class="form-group row">
                                <label for="phoneName" class="col-sm-2 col-form-label text-right">Name : </label>
                                <div class="col-sm-10">
                                    <input type="text" id="phoneName" placeholder="Ex : Samsung Galaxy S7"
                                    @if ($mode == 'show')
                                      class="form-control-plaintext" readonly value="Samsung Galaxy S7"
                                    @else
                                      class="form-control"
                                    @endif>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="manufacture" class="col-sm-2 col-form-label text-right">Manufacture : </label>
                                <div class="col-sm-10">
                                    <select name="manufacture" id="manufacture" class="form-control select2" 
                                    @if ($mode == 'show')
                                      disabled
                                    @endif>

                                        <option
                                        @if ($mode == 'show')
                                          selected
                                        @endif>Samsung</option>

                                        <option>Oppo</option>
                                        <option>Vivo</option>
                                        <option>Apple</option>
                                        <option>Oneplus</option>
                                        <option>Xiaomi</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="productionYear" class="col-sm-2 col-form-label text-right">Production Year : </label>
                                <div class="col-sm-10">
                                    <select name="manufacture" id="productionYear" class="form-control select2"
                                    @if ($mode == 'show')
                                      disabled
                                    @endif>
                                        <option>2008</option>
                                        <option>2009</option>
                                        <option>2010</option>
                                        <option>2011</option>
                                        <option>2012</option>
                                        <option>2013</option>
                                        <option>2014</option>

                                        <option 
                                        @if ($mode == 'show')
                                          selected
                                        @endif>2015</option>

                                        <option>2016</option>
                                        <option>2017</option>
                                        <option>2018</option>
                                        <option>2019</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="phoneName" class="col-sm-2 col-form-label text-right">Condition : </label>
                                <div class="col-sm-10">
                                    <input type="text" id="phoneName" placeholder="Ex : Samsung Galaxy S7" class="form-control-plaintext" 
                                    readonly value="Sekond">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3">
                      <div class="card-header">
                          <i class="fas fa-info-circle"></i> Condition Detail
                      </div>
                      <div class="card-body">
                          <table class="table">
                            <thead>
                              <tr>
                                <th>Part</th>
                                <th>Condition</th>
                                <th>Photos</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>Display</td>
                                <td>Retak, Shadow</td>
                                <td><button class="btn btn-info" type="button" data-toggle="modal" data-target="#photos">V</button></td>

                                <div class="modal fade" id="photos">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <img class="mx-auto d-block mb-2" width="auto" src="{{asset('storage/asset/phone_placeholder.jpg')}}">
                                        <img class="mx-auto d-block mb-2" width="auto" src="{{asset('storage/asset/phone_placeholder.jpg')}}">
                                        <img class="mx-auto d-block mb-2" width="auto" src="{{asset('storage/asset/phone_placeholder.jpg')}}">
                                        <img class="mx-auto d-block mb-2" width="auto" src="{{asset('storage/asset/phone_placeholder.jpg')}}">
                                        <img class="mx-auto d-block mb-2" width="auto" src="{{asset('storage/asset/phone_placeholder.jpg')}}">
                                        <img class="mx-auto d-block mb-2" width="auto" src="{{asset('storage/asset/phone_placeholder.jpg')}}">
                                      </div>
                                    </div>
                                  </div>
                              </tr>
                              <tr>
                                <td>Speaker</td>
                                <td>Kecil</td>
                                <td></td>
                              </tr>
                            </tbody>
                          </table>
                      </div>
                    </div>

                    <div class="card">
                      <div class="card-header">
                          <i class="fas fa-info-circle"></i> Price Analysist
                      </div>
                      <div class="card-body">
                        <h3 class="text-center">Price : Rp. 5.000.000</h3>
                        <hr>
                        <div class="row">
                          <div class="col-6">
                            <h2 class="text-center text-success">GOOD</h2>
                            <div class="text-center">
                              <span>(Rank </span> <span class="text-success h5">#4</span> <span>from </span> <span class="text-success h5">#215</span> <span> on same condition)</span>
                            </div>
                            {{-- <h5 class="text-center text-success">(Rank #4 from #215 on same condition phone)</h5> --}}
                          </div>
                          <div class="col-6">
                            <h2 class="text-center text-warning">MEDIUM</h2>
                            <div class="text-center">
                              <span>(Rank </span> <span class="text-warning h5">#400</span> <span>from </span> <span class="text-warning h5">#500</span> <span> on same phone)</span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </form>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="{{ url('/samsung-galaxy-s7') }}">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Page level plugin JavaScript-->
  <script src="{{asset('vendor/chart.js/Chart.min.js')}}"></script>
  <script src="{{asset('vendor/datatables/jquery.dataTables.js')}}"></script>
  <script src="{{asset('vendor/datatables/dataTables.bootstrap4.js')}}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{asset('js/sb-admin.min.js')}}"></script>

  <!-- Demo scripts for this page-->
  <script src="{{asset('js/demo/datatables-demo.js')}}"></script>
  <script src="{{asset('js/demo/chart-area-demo.js')}}"></script>

  {{-- select 2 js --}}
  <script src="{{asset('js/select2.min.js')}}"></script>

  {{-- chartjs --}}
  <script src="{{asset('js/moment.min.js')}}"></script>
  <script src="{{asset('js/chart.min.js')}}"></script>

  <script>
    $(document).ready(function() {
        $('.select2').select2({
            theme: "bootstrap",
            width: '100%'
        });
    });

    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ['Jan 2019', 'Feb 2019', 'Mar 2019', 'Apr 2019', 'May 2019', 'Jun 2019', 'Jul 2019', 'Aug 2019', 'Sep 2019'],
        datasets: [{
            label: 'Price',
            data: [2000000, 1950000, 1752000, 1500000, 1500000, 1650000, 1450000, 1050000, 1250000],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }});
  </script>

</body>

</html>

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.search');
});

Route::get('/samsung-galaxy-s7', function () {
    return view('backend.add_product', ['mode' => 'show']);
});

Route::get('/samsung-galaxy-s7/123', function () {
    return view('backend.phone_detail', ['mode' => 'show']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin/add-product', function () {
    return view('backend.add_product', ['mode' => 'add']);
});

Route::get('/admin/add-data', function () {
    return view('backend.add_data');
});
